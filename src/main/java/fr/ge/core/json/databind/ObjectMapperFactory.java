/**
 * 
 */
package fr.ge.core.json.databind;

import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Jackson's ObjectMapper factory to create a mapper with custom Deserialization
 * configuration.
 * 
 * @author $Author: LABEMONT $
 */
public class ObjectMapperFactory {

    /**
     * Create an json object mapper based on an existing one, with added
     * configuration
     * 
     * @param baseObjectMapper
     *            the existing object mapper, not null
     * @param deactivatedDeserializationFeatures
     *            the added configuration to deactivate, not null
     * @return
     */
    public static ObjectMapper create(ObjectMapper baseObjectMapper, List<DeserializationFeature> deactivatedDeserializationFeatures) {

        ObjectMapper objectMapper = baseObjectMapper.copy();
        for (DeserializationFeature feature : deactivatedDeserializationFeatures) {
            objectMapper.configure(feature, false);
        }

        return objectMapper;
    }

}

/**
 * 
 */
package fr.ge.core.rest.exception;

import javax.ws.rs.core.Response.Status;

/**
 * Exception to handle the return code on error
 * 
 * @author $Author: LABEMONT $
 */
public class RestResponseException extends Throwable {

    /** serial id. */
    private static final long serialVersionUID = -994545711524935710L;

    private String message;

    private Status status;

    /**
     * Constructeur de la classe.
     *
     * @param message
     * @param status
     */
    public RestResponseException(String message, Status status) {
        super();
        this.message = message;
        this.status = status;
    }

    /**
     * Accesseur sur l'attribut {@link #message}.
     *
     * @return String message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Accesseur sur l'attribut {@link #status}.
     *
     * @return Status status
     */
    public Status getStatus() {
        return status;
    }

}

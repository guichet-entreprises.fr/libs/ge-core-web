/**
 * 
 */
package fr.ge.core.rest.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import fr.ge.core.rest.exception.RestResponseException;

/**
 * Creating a response from a custom exception.
 * 
 * @author $Author: loic.abemonty $
 */
public class RestResponseExceptionMapper implements ExceptionMapper<RestResponseException> {

    /**
     * Use the exception status to create a response {@inheritDoc}
     */
    @Override
    public Response toResponse(RestResponseException exception) {

        Response response = Response.status(Status.EXPECTATION_FAILED).build();
        if (exception != null && exception.getStatus() != null) {
            response = Response.status(exception.getStatus()).entity(exception.getMessage()).build();
        }
        return response;
    }

}

package fr.ge.core.rest;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;
import org.apache.cxf.jaxrs.impl.ResponseImpl;
import org.slf4j.Logger;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.log.GestionnaireTrace;

/**
 * Generic class for managing exception according to http code response. Technical exception is
 * raised for the following HTTP code error : 400, 405 (and more), 5xx and functional exception is
 * raised for the following HTTP code error : 401, 402, 403, 404
 */
public class RestClientExceptionMapper implements ResponseExceptionMapper < Throwable > {

  /** Functionnal logger. **/
  private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** Technical logger. **/
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  @Override
  public Throwable fromResponse(Response response) {
    // -->Getting information from response
    final ResponseImpl responseImpl = (ResponseImpl) response;
    final String method = (String) responseImpl.getOutMessage().getExchange().get("org.apache.cxf.resource.operation.name");
    final String endPointAdress = (String) responseImpl.getOutMessage().getExchange()
      .get(org.apache.cxf.message.Message.ENDPOINT_ADDRESS);

    // -->Get HTTP code return
    Response.Status status = Response.Status.fromStatusCode(response.getStatus());

    // -->Creating message exception
    final String messageException = String.format("Status: %s, method: %s, Endpoint adress: %s", status.getStatusCode(), method,
      endPointAdress);

    switch (status) {
      case UNAUTHORIZED:
      case PAYMENT_REQUIRED:
      case FORBIDDEN:
      case NOT_FOUND:
        LOGGER_FONC.error(String.format("Functional exception raised with %s", messageException));
        return new FunctionalException(messageException);

      case BAD_REQUEST:
      case METHOD_NOT_ALLOWED:
        LOGGER_TECH.error(String.format("Technical exception raised with %s", messageException));
        return new TechniqueException(messageException);
      default:
        LOGGER_TECH.error(String.format("Default exception raised with %s", messageException));
        return new TechniqueException(messageException);
    }
  }

}

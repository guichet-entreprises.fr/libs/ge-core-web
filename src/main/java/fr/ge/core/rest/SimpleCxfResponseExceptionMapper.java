package fr.ge.core.rest;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;
import org.apache.cxf.jaxrs.impl.ResponseImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic class that throws a parameterized exception. The Exception class must
 * have a constructor with one parameter, a simple String.
 * 
 * @param <E>
 *            the Exception class
 */
public class SimpleCxfResponseExceptionMapper<E extends Throwable> implements ResponseExceptionMapper<Throwable> {

    /** logger. **/
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCxfResponseExceptionMapper.class);

    /** Exception class */
    private Class<E> exceptionClass;

    @Override
    public Throwable fromResponse(Response response) {
        // -->Getting information from response
        final ResponseImpl responseImpl = (ResponseImpl) response;
        final String method = (String) responseImpl.getOutMessage().getExchange().get("org.apache.cxf.resource.operation.name");
        final String endPointAdress = (String) responseImpl.getOutMessage().getExchange().get(org.apache.cxf.message.Message.ENDPOINT_ADDRESS);

        // -->Get HTTP code return
        Response.Status status = Response.Status.fromStatusCode(response.getStatus());

        // -->Creating message exception
        final String messageException = String.format("Status: %s, method: %s, Endpoint adress: %s", status.getStatusCode(), method, endPointAdress);

        LOGGER.error(String.format("Default exception raised with %s", messageException));
        E exception = createException(messageException);
        if (exception == null) {
            return new RuntimeException(messageException);
        }
        return exception;

    }

    /**
     * Create a exception from the parameterized class with the custom message.
     * 
     * @param message
     *            the message
     * @return the exception, null if unable to create
     */
    private E createException(String message) {
        if (exceptionClass != null) {
            try {
                Constructor<E> constructor = exceptionClass.getDeclaredConstructor(String.class);
                return constructor.newInstance(message);
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                LOGGER.warn("Unable to create an exception from {}, because : {}.", exceptionClass, e.getMessage());
                return null;
            }
        }
        LOGGER.warn("Unable to create an exception from {}.", exceptionClass);
        return null;
    }

    /**
     * Mutateur sur l'attribut {@link #exceptionClass}.
     *
     * @param exceptionClass
     *            la nouvelle valeur de l'attribut exceptionClass
     */
    public void setExceptionClass(Class<E> exceptionClass) {
        this.exceptionClass = exceptionClass;
    }

}

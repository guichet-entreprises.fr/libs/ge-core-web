package fr.ge.core.rest;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.util.LinkedList;
import java.util.List;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;

/**
 * Test de la gestion des retours REST par Exception
 * 
 * @author $Author: LABEMONT $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/ge-core-web-cxf-client-config.xml" })
public class SimpleCxfResponseExceptionMapperTest {

    /** End point adress. */
    private static final String ENDPOINT_ADDRESS = "local://calculator/";

    /** The server. */
    private static Server server;

    @Autowired
    private SimpleCxfResponseExceptionMapper defaultResponseMapper;

    /**
     * Initialize the test class.
     */
    @BeforeClass
    public static void initialize() {
        startServer();
    }

    /**
     * Start the server.
     */
    private static void startServer() {
        JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
        sf.setResourceClasses(Calculator.class);
        List<Object> providers = new LinkedList<Object>();
        SimpleCxfResponseExceptionMapper exceptionMapper = new SimpleCxfResponseExceptionMapper<>();
        exceptionMapper.setExceptionClass(FunctionalException.class);
        providers.add(exceptionMapper);
        providers.add(new JacksonJaxbJsonProvider());
        sf.setResourceProvider(Calculator.class, new SingletonResourceProvider(new CalculatorImpl()));
        sf.setProviders(providers);
        sf.setAddress(ENDPOINT_ADDRESS);
        server = sf.create();
    }

    /**
     * Stop and Destroy the server at the end.
     */
    @AfterClass
    public static void destroy() {
        server.stop();
        server.destroy();
    }

    @Test
    public void testFromResponse500() throws TechniqueException {
        List<Object> providers = new LinkedList<Object>();
        // SimpleCxfResponseExceptionMapper exceptionMapper = new
        // SimpleCxfResponseExceptionMapper<>();
        // exceptionMapper.setExceptionClass(FunctionalException.class);
        providers.add(defaultResponseMapper);
        Calculator calculator = JAXRSClientFactory.create(ENDPOINT_ADDRESS, Calculator.class, providers);

        Exception executionException = null;

        try {
            calculator.divide(4, 0);
            fail();
        } catch (FunctionalException e) {
            executionException = e;
        }
        assertThat(executionException).isNotNull();
        assertThat(executionException.getMessage()).isEqualTo("Status: 500, method: divide, Endpoint adress: " + ENDPOINT_ADDRESS);
    }

}

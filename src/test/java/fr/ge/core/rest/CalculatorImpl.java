package fr.ge.core.rest;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;

@WebService
public class CalculatorImpl implements Calculator {

  public CalculatorImpl() {

  }

  @Override

  @GET
  @Path("/divide/{a}/{b}")
  public double divide(double a, double b) throws FunctionalException, TechniqueException {
    if (b == 0) {
      throw new TechniqueException("The argument is illegal");
    }

    return a / b;
  }
}

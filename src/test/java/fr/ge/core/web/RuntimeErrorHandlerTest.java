package fr.ge.core.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RuntimeErrorHandlerTest {

  @InjectMocks
  private RuntimeErrorHandler runtimeErrorHandler;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test doGet.
   * 
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void testDoGet() throws ServletException, IOException {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    Mockito.when(request.getRequestURI()).thenReturn("http://www.google.fr");
    List < String > list = new ArrayList < String >();
    Mockito.when(request.getAttributeNames()).thenReturn(Collections.enumeration(list));
    Mockito.when(request.getHeaderNames()).thenReturn(Collections.enumeration(list));
    Mockito.when(request.getLocale()).thenReturn(Locale.FRENCH);
    PrintWriter out = Mockito.mock(PrintWriter.class);
    Mockito.when(response.getWriter()).thenReturn(out);
    Mockito.doNothing().when(out).println(Matchers.anyString());

    runtimeErrorHandler.doGet(request, response);

    Mockito.when(request.getLocale()).thenReturn(Locale.ENGLISH);
    runtimeErrorHandler.doGet(request, response);
  }

  /**
   * Test doPost.
   * 
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void testDoPost() throws ServletException, IOException {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    Mockito.when(request.getRequestURI()).thenReturn("http://www.google.fr");
    List < String > list = new ArrayList < String >();
    Mockito.when(request.getAttributeNames()).thenReturn(Collections.enumeration(list));
    Mockito.when(request.getHeaderNames()).thenReturn(Collections.enumeration(list));
    Mockito.when(request.getLocale()).thenReturn(Locale.FRENCH);
    PrintWriter out = Mockito.mock(PrintWriter.class);
    Mockito.when(response.getWriter()).thenReturn(out);
    Mockito.doNothing().when(out).println(Matchers.anyString());

    runtimeErrorHandler.doPost(request, response);

    Mockito.when(request.getLocale()).thenReturn(Locale.ENGLISH);
    runtimeErrorHandler.doPost(request, response);
  }

}

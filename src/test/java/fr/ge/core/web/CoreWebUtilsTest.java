package fr.ge.core.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * JUNIT for testing core web utility methods.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class CoreWebUtilsTest {

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test encoding URL with success.
   * 
   * @throws Exception
   */
  @Test
  public void testEncodeURL() throws Exception {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    final String currentUrl = "http://www.google.fr";
    Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer(currentUrl));
    final String actual = CoreWebUtils.encodeURL(request);
    final String expected = URLEncoder.encode(currentUrl, StandardCharsets.UTF_8.toString());
    assertEquals(expected, actual);
  }

  /**
   * 
   * Test encoding URL with exception.
   * 
   * @throws Exception
   */
  @Test
  public void testEncodeURLException() throws Exception {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    final String actual = CoreWebUtils.encodeURL(request);
    assertNull(actual);
  }

}

/**
 * 
 */
package fr.ge.core.json.databind;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * test objectmapper factory.
 * 
 * @author $Author: LABEMONT $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/ge-core-web-cxf-client-config.xml")
public class ObjectMapperFactoryTest {

    @Autowired
    private ObjectMapper javaJaxbJsonMapper;

    @Autowired
    private ObjectMapper classicJaxbJsonMapper;

    /**
     * Test qu'une feature est désactivée.
     */
    @Test
    public void deserializationDeactivationTest() {
        assertThat(javaJaxbJsonMapper).isNotNull();
        assertThat(javaJaxbJsonMapper.isEnabled(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)).isTrue();

        assertThat(classicJaxbJsonMapper).isNotNull();
        assertThat(classicJaxbJsonMapper.isEnabled(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)).isFalse();
    }

}
